<?php

namespace Drupal\group_forum_discrete\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\forum\ForumManagerInterface;
use Drupal\group\Entity\GroupRelationshipInterface;
use Drupal\taxonomy\TermStorageInterface;

/**
 * Breadcrumb builder for forum nodes.
 */
class ForumNodeBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  /**
   * @var \Drupal\taxonomy\TermStorageInterface $termStorage
   */
  protected TermStorageInterface $termStorage;

  /**
   * Constructs a forum breadcrumb builder object.
   *
   * @param \Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface $originalBuilder
   *   The forum module breadcrumb builder we are decorating.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\forum\ForumManagerInterface $forumManager
   *   The forum manager service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(protected BreadcrumbBuilderInterface $originalBuilder, protected EntityTypeManagerInterface $entityTypeManager, protected ConfigFactoryInterface $configFactory, protected ForumManagerInterface $forumManager, TranslationInterface $string_translation) {
    $this->setStringTranslation($string_translation);
    $this->termStorage = $entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return $this->originalBuilder->applies($route_match);
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addCacheContexts(['route']);
    $links[] = Link::createFromRoute($this->t('Home'), '<front>');

    $parents = $this->termStorage->loadAllParents($route_match->getParameter('node')->taxonomy_forums?->target_id);
    $group_relationships = [];
    if (!empty($parents)) {
      foreach ($parents as $parent) {
        $group_relationships = _group_forum_load_group_relationship($parent);
        if (!empty($group_relationships)) {
          break;
        }
      }
    }
    $vocabulary = $this->entityTypeManager
      ->getStorage('taxonomy_vocabulary')
      ->load($this->configFactory->get('forum.settings')->get('vocabulary'));
    $breadcrumb->addCacheableDependency($vocabulary);
    if (empty($group_relationships)) {
      $links[] = Link::createFromRoute($vocabulary->label(), 'forum.index');
    }
    else {
      $primary_relationship = reset($group_relationships);
      assert($primary_relationship instanceof GroupRelationshipInterface);
      $group = $primary_relationship->getGroup();
      $breadcrumb->addCacheableDependency($group);
      $links[] = $group->toLink();
      $links[] = Link::createFromRoute($vocabulary->label(), 'entity.group.group_forum', ['group' => $group->id()]);
    }

    if ($parents) {
      $parents = array_reverse($parents);
      foreach ($parents as $parent) {
        $breadcrumb->addCacheableDependency($parent);
        $links[] = Link::createFromRoute($parent->label(), 'forum.page',
          [
            'taxonomy_term' => $parent->id(),
          ]
        );
      }
    }

    return $breadcrumb->setLinks($links);
  }

}
