<?php

namespace Drupal\group_forum_discrete\Controller;

use Drupal\forum\Controller\ForumController as CoreForumController;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\Storage\GroupRelationshipStorageInterface;

/**
 * Controller for group filtered forum routes.
 */

class ForumController extends CoreForumController {

  /**
   * Returns forum index page.
   *
   * @return array
   *   A render array.
   */
  public function groupForumIndex(GroupInterface $group) {
    // Find all term ids that fall under this group.
    $group_forum_ids = [];
    $vocabulary = $this->vocabularyStorage->load($this->config('forum.settings')->get('vocabulary'));
    $group_relationship_storage = $this->entityTypeManager()->getStorage('group_content');
    assert($group_relationship_storage instanceof GroupRelationshipStorageInterface);
    $group_relationships = $group_relationship_storage->loadByGroup($group, 'group_forum');
    foreach ($group_relationships as $group_relationship) {
      $term = $group_relationship->getEntity();
      $tree = $this->termStorage->loadTree($vocabulary->id(), $term->id());
      $group_forum_ids = array_merge($group_forum_ids, [$term->id()], array_column($tree, 'tid'));
    }
    $group_forum_ids = array_combine($group_forum_ids, $group_forum_ids);

    // If there is a table with just one row, redirect to the single forum.
    if (count($group_forum_ids) == 1) {
      return $this->redirect('forum.page', ['taxonomy_term' => reset($group_forum_ids)]);
    }

    // Standard forum index.
    $index = $this->forumManager->getIndex();
    // But filtered by this groups tids.
    $index->forums = array_intersect_key($index->forums, $group_forum_ids);
    $build = $this->build($index->forums, $index);
    // And use groups add and relate actions.
    unset($build['action']);
    if (empty($index->forums)) {
      // Root of empty forum.
      $build['#title'] = $this->t('No forums defined');
    }
    else {
      // Set the page title to forum's vocabulary name.
      $build['#title'] = $vocabulary->label();
      $this->renderer->addCacheableDependency($build, $vocabulary);
    }
    return $build;
  }

}
